<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_codeline');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'loyboy');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iF?BaI;Ji[tQx?1kv0V$p(G~2gLv.MZVK4%E&O~mH}#z,xwY,FY&&s/{oYEWroRZ');
define('SECURE_AUTH_KEY',  'QJ7clMg}%`#bH[vEdF]xYPESW33_>(MdKI;OA.0jx$x|5BG:_g<qT#34a21SNG6y');
define('LOGGED_IN_KEY',    ')CeiOY`GQ|yiNVg$_I#fL1knP<M]jLjL=!<akg{Jt?[ik3QQcS#IbwW{[Mxt?)E7');
define('NONCE_KEY',        'F;7Q7B zF~<ym0)h,dGT MD#Vk~$r9OJ>DPn%[]gHD6c9-b):r#Y9 $MX4|68YKV');
define('AUTH_SALT',        'y`[R|J@y{C@%is8:4mkHqo4}S=X>,Sh-vy(6P:M%47pu1QnCa^V;:cR9g]8,@$Sv');
define('SECURE_AUTH_SALT', 'yN*{(No@9LwbaFiaDj;RD&``mIf+4/o]CBpKw5np;Ug#A9%(R.(#l[e6a&iZl;Ak');
define('LOGGED_IN_SALT',   'B:WhuN=aME,IBD-#.|RwwJS%n`-4g/U(]+XVz/yF.pcF%eS`C%<uu)]P!~;nw?X=');
define('NONCE_SALT',       '+,4)l0qVlXnib1mjeFP-6xf)CjVT<s]AcZ[}R!&@`<6Q~q_#4]3-5BfV-}2hw2i=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
