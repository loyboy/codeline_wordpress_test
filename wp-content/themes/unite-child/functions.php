<?php
//-------------------------------------------------------------------
add_action( 'wp_enqueue_scripts', 'get_parent_styles' );

////you need to get the parent style to display within the Child Theme
function get_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

//Add the function for New Post type ---- 'Films'
//-------------------------------------------------------------------
add_action( 'init', 'lc_custom_film_post_type' );

// A custom function that calls register_post_type
function lc_custom_film_post_type() {

  // Set various pieces of text, $labels is used inside the $args array
  $labels = array(
     'name'               => __( 'Films' ),
    'singular_name'      => __( 'Film' ),
    'add_new'            => __( 'Add New Film' ),
    'add_new_item'       => __( 'Add New Film' ),
    'edit_item'          => __( 'Edit Film' ),
    'new_item'           => __( 'New Film' ),
    'all_items'          => __( 'All Films' ),
    'view_item'          => __( 'View Film' ),
    'search_items'       => __( 'Search Films' ),
    'featured_image'     => 'Film Poster',
    'set_featured_image' => 'Add Film Poster'
  );

  // Set various pieces of information about the post type
  $args = array(
    'labels'            => $labels,
    'description'       => 'The films Codeline needs me to store',
    'public'            => true,
    'menu_position'     => 4,
    'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields' ),
    'has_archive'       => true,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'has_archive'       => true,
    'query_var'         => 'film'
  );
	
	// Register the film post type with all the information contained in the $arguments array
  register_post_type( 'film', $args );}

//--------------------------------------------------------
//You should then set the taxonomies
function my_taxonomies_film() {
  $args_genre = array(
    // Hierarchical taxonomy (like custom categories)
    'hierarchical' => true,
    // for the Admin to understand :-)
    'labels' => array(
      'name' => __( 'Genres'),
      'singular_name' => __( 'Genre' ),
      'search_items' =>  __( 'Search Genres' ),
      'all_items' => __( 'All Genres' ),
      'parent_item' => __( 'Parent Genre' ),
      'parent_item_colon' => __( 'Parent Genre:' ),
      'edit_item' => __( 'Edit Genre' ),
      'update_item' => __( 'Update Genre' ),
      'add_new_item' => __( 'Add New Genre' ),
      'new_item_name' => __( 'New Genre Name' ),
      'menu_name' => __( 'Genres' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'filmgenre', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/filmgenre/"
      'hierarchical' => true 
    )
  );
	
  $args_country = array(
    // Hierarchical taxonomy (like custom categories)
    'hierarchical' => true,
    // for the Admin to understand :-)
    'labels' => array(
      'name' => __( 'Country'),
      'singular_name' => __( 'Country' ),
      'search_items' =>  __( 'Search Country' ),
      'all_items' => __( 'All Countries' ),
      'parent_item' => __( 'Parent Country' ),
      'parent_item_colon' => __( 'Parent Country:' ),
      'edit_item' => __( 'Edit Country' ),
      'update_item' => __( 'Update Country' ),
      'add_new_item' => __( 'Add New Country' ),
      'new_item_name' => __( 'New Country Name' ),
      'menu_name' => __( 'Countries' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'filmcountry', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/filmcountry/"
      'hierarchical' => true 
    )
  );	
	
$args_year = array(
    // Hierarchical taxonomy (like custom categories)
    'hierarchical' => true,
    // for the Admin to understand :-)
    'labels' => array(
      'name' => __( 'Year'),
      'singular_name' => __( 'Year' ),
      'search_items' =>  __( 'Search Year' ),
      'all_items' => __( 'All The Years' ),
      'parent_item' => __( 'Parent Year' ),
      'parent_item_colon' => __( 'Parent Year:' ),
      'edit_item' => __( 'Edit Year' ),
      'update_item' => __( 'Update Year' ),
      'add_new_item' => __( 'Add New Year' ),
      'new_item_name' => __( 'New Year Date' ),
      'menu_name' => __( 'Year' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'filmyear', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/filmyear/"
      'hierarchical' => true 
    )
  );
	
	$args_actor = array(
    // Hierarchical taxonomy (like custom categories)
    'hierarchical' => true,
    // for the Admin to understand :-)
    'labels' => array(
      'name' => __( 'Actors'),
      'singular_name' => __( 'Actor' ),
      'search_items' =>  __( 'Search Actors' ),
      'all_items' => __( 'All The Actors' ),
      'parent_item' => __( 'Parent Actor' ),
      'parent_item_colon' => __( 'Parent Actor:' ),
      'edit_item' => __( 'Edit Actor' ),
      'update_item' => __( 'Update Actor' ),
      'add_new_item' => __( 'Add New Actor' ),
      'new_item_name' => __( 'New Actor Name' ),
      'menu_name' => __( 'Actors' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'filmactor', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/filmactor/"
      'hierarchical' => true 
    )
  );
  register_taxonomy( 'Genre', 'film', $args_genre );
  register_taxonomy( 'Country', 'film', $args_country );
  register_taxonomy( 'Year', 'film', $args_year );
  register_taxonomy( 'Actors', 'film', $args_actor );
}

add_action( 'init', 'my_taxonomies_film', 0 );

//--------------------------------------------------------------
//Make the Films show on the front page
// Hook our custom function to the pre_get_posts action hook
add_action( 'pre_get_posts', 'add_films_to_frontpage' );
 
// Alter the main query
function add_films_to_frontpage( $query ) {
    if ( is_home() && $query->is_main_query() ) {
        $query->set( 'post_type', array( 'film') );
		
    }
    return $query;
}

//Add hook to change Films list
// Hook our custom function to the change_film_posts action hook
add_action('change_film_posts', 'change_film_posts_pre');
//
function change_film_posts_pre() { 
	if(in_the_loop()) $post_id = get_the_ID();
    else $post_id = get_queried_object_id();
			echo "<p style='margin-top: 10px;'>";
			echo get_the_term_list( $post_id, 'Genre', 'Genre: ', ', ' ); echo "<br>";
			echo get_the_term_list( $post_id, 'Country', 'Country: ', ', ' ); echo "<br>";
			echo "Ticket Price: ". get_post_meta( $post_id, 'ticket_price', TRUE ); echo "<br>";
			echo "Release Date: ". get_post_meta( $post_id, 'release_date', TRUE );echo "<br>";
			echo "</p>";
}

//Add the Shortcode hook and Function to display Just 5 latest films
function register_shortcodes_films() {
    add_shortcode('film_query', 'film_shortcode_query');
}
add_action( 'init', 'register_shortcodes_films' );

function film_shortcode_query(){
 		
		$args = array(
            'post_type' => 'film',
            'posts' => '5',
            'post_status' => 'publish'
            
        );
	

        $query = new WP_Query( $args );
        if( $query->have_posts() ){
           $out .= "<ul>";
            while( $query->have_posts() ){
                $query->the_post();
                
            $out .= '<div class="film_box">
             <h4>Film Name: <a href="'.get_permalink().'" title="' . get_the_title() . '">'.get_the_title() .'</a></h4>';
            $out .='</div>';

            }
			
			$out .= "</ul>";
           
        }
	else{
    return; // no posts found 
	}

	  wp_reset_query();
	  return html_entity_decode($out);
       
}

?>